# Banbot

The Bangbot is our Bot at @rustlangbr designed to prevent bots from entering
and spamming the channel.

When a new user enters the server, the bot will do three things:

1. Ban the new user if the first message contains a link or image.
2. Kick the new user if he does not interact within the first 24 hours after entering the group.
3. If no one greets the new user in at least 2 hours, the Bot itself will greet the user.
